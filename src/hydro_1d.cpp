/**
 * @file hydro_1d.cpp
 * @author Omry Cohen
 * @brief This code compiles into a python hydro_1d package using pybind
 * @version 0.1
 * @date 28-04-2020
 * 
 * @copyright Copyright (c) 2020
 * 
 */

# include <memory>
# include <pybind11/pybind11.h>
# include <pybind11/stl.h>
# include "geometry.hpp"
# include "hydro_data.hpp"
# include "hydro_driver.hpp"


namespace py = pybind11;


PYBIND11_MODULE(hydro_1d, m) {
    using namespace pybind11::literals;

    // Geometry pybinding
    py::class_<Geometry, std::shared_ptr<Geometry>>(m, "Geometry")
        .def(py::init<double, double>(), "alpha"_a, "beta"_a)
        .def_readonly("alpha", &Geometry::alpha)
        .def_readonly("beta",  &Geometry::beta);

    // Geometry instances binding
    m.attr("planar")      = &geometries::PLANAR;
    m.attr("spherical")   = &geometries::SPHERICAL;
    m.attr("cylindrical") = &geometries::CYLINDRICAL;

    // HydroData pybinding
    py::class_<HydroData, std::shared_ptr<HydroData>>(m, "HydroData")
        .def(py::init<Geometry const,
                      std::vector<double> const,
                      std::vector<double> const,
                      std::vector<double> const,
                      std::vector<double> const>(),
                      "geometry"_a,
                      "position"_a,
                      "velocity"_a,
                      "pressure"_a,
                      "rho"_a)
        .def_readonly("position",             &HydroData::position)
        .def_readonly("velocity",             &HydroData::velocity)
        .def_readonly("acceleration",         &HydroData::acceleration)
        .def_readonly("pressure",             &HydroData::pressure)
        .def_readonly("mass",                 &HydroData::mass)
        .def_readonly("vertex_mass",          &HydroData::vertex_mass)
        .def_readonly("volume",               &HydroData::volume)
        .def_readonly("rho",                  &HydroData::rho)
        .def_readonly("artificial_viscosity", &HydroData::artificial_viscosity);


    // HydroDriver pybinding
    py::class_<HydroDriver, std::shared_ptr<HydroDriver>>(m, "HydroDriver")
        .def(py::init<Geometry const, 
                      double const, 
                      std::vector<double> const,
                      std::vector<double> const,
                      std::vector<double> const,
                      std::vector<double> const,
                      double const,
                      double const,
                      double const,
                      double const,
                      double const>(),
                      "geometry"_a,
                      "gamma"_a,
                      "init_position"_a,
                      "init_velocity"_a,
                      "init_pressure"_a,
                      "init_rho"_a,
                      "artificial_viscosity_sigma"_a,
                      "max_initial_dt"_a,
                      "dt_factor"_a,
                      "dt_density_epsilon"_a,
                      "dt_courant_factor"_a)
        .def_readonly("geometry",                   &HydroDriver::geometry)
        .def_readonly("gamma",                      &HydroDriver::gamma)
        .def_readonly("data",                       &HydroDriver::data)
        .def_readonly("old_data",                   &HydroDriver::old_data)
        .def_readonly("dt",                         &HydroDriver::dt)
        .def_readonly("dt_factor",                  &HydroDriver::dt_factor)
        .def_readonly("artificial_viscosity_sigma", &HydroDriver::artificial_viscosity_sigma)
        .def_readonly("dt_density_epsilon",         &HydroDriver::dt_density_epsilon)
        .def         ("step",                       &HydroDriver::step)
        .def         ("dt_courant",                 &HydroDriver::dt_courant)
        .def         ("dt_density",                 &HydroDriver::dt_density);

}

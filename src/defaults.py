artificial_viscosity_sigma = 1.
dt_factor                  = 1.1
dt_density_epsilon         = 1e-2 
dt_courant_factor          = 0.25

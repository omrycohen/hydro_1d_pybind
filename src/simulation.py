import numpy as np
import h5py as h5
from datetime import datetime
import os 
import sys
# from console_progressbar import ProgressBar

# get this file's ('test.py') path
project_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# import pybind11 generated module 
sys.path.append(f"{project_path}")
import build.Debug.hydro_1d as hydro
import src.defaults as defaults

geometries = {"planar"      : hydro.planar,
              "spherical"   : hydro.spherical,
              "cylindrical" : hydro.cylindrical}

try:
    os.mkdir(f"{project_path}/out")
except:
    pass # Directory already exists

class Simulation():
    def __init__(self, 
                 name,
                 geometry, 
                 gamma, 
                 init_position,
                 init_velocity,
                 init_pressure,
                 init_rho,
                 dt_writing,
                 max_initial_dt,
                 dt_factor                  = defaults.dt_factor,
                 artificial_viscosity_sigma = defaults.artificial_viscosity_sigma,
                 dt_density_epsilon         = defaults.dt_density_epsilon,
                 dt_courant_factor          = defaults.dt_courant_factor):
        self.hydro_driver = hydro.HydroDriver(geometry                   = geometry, 
                                              gamma                      = gamma, 
                                              init_position              = init_position,
                                              init_velocity              = init_velocity,
                                              init_pressure              = init_pressure,
                                              init_rho                   = init_rho,
                                              max_initial_dt             = max_initial_dt,
                                              dt_factor                  = dt_factor,
                                              artificial_viscosity_sigma = artificial_viscosity_sigma,
                                              dt_density_epsilon         = dt_density_epsilon,
                                              dt_courant_factor          = dt_courant_factor)
        self.name = name
        self.time = 0.
        self.h5filename = None
        self.h5file = None
        self.dt_writing = dt_writing
        # self.pb = ProgressBar(total=100, prefix='Here', suffix='Now', decimals=3, length=50, fill='X', zfill='-')

        self.initialize_h5()
        self.write_to_h5()


    def initialize_h5(self):
        now = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
        number_of_cells = len(self.hydro_driver.data.mass)
        self.h5filename = f"{project_path}/out/{self.name}_{now}.hdf5"
        self.h5file = h5.File(self.h5filename, "w")
        self.h5file.attrs["name"] = self.name
        self.h5file.create_dataset("time",                 (0,),                     dtype='float64', maxshape = (None,))
        self.h5file.create_dataset("position",             (0, number_of_cells + 1), dtype='float64', maxshape = (None, number_of_cells + 1))
        self.h5file.create_dataset("velocity",             (0, number_of_cells + 1), dtype='float64', maxshape = (None, number_of_cells + 1))
        self.h5file.create_dataset("pressure",             (0, number_of_cells),     dtype='float64', maxshape = (None, number_of_cells))
        self.h5file.create_dataset("rho",                  (0, number_of_cells),     dtype='float64', maxshape = (None, number_of_cells))    
        self.h5file.create_dataset("artificial_viscosity", (0, number_of_cells),     dtype='float64', maxshape = (None, number_of_cells))


    def write_to_h5(self):

        vars = {"position"             : self.hydro_driver.data.position,
                "velocity"             : self.hydro_driver.data.velocity, 
                "pressure"             : self.hydro_driver.data.pressure, 
                "rho"                  : self.hydro_driver.data.rho,
                "artificial_viscosity" : self.hydro_driver.data.artificial_viscosity}

        ds = self.h5file["time"]
        ds.resize(tuple(map(sum, zip(ds.shape, (1,)))))
        ds[-1] = self.time

        for var in vars:
            ds = self.h5file[var]
            ds.resize(tuple(map(sum, zip(ds.shape, (1,0)))))
            ds[-1, :] = vars[var]


    def mainloop(self, max_time):

        percentage = last_percentage = 0
        last_writing_time = 0.
        print("0%")

        while self.time < max_time:

            # Print percentage
            percentage = int(100 * self.time / max_time)
            if percentage > last_percentage:
                # self.pb.print_progress_bar(percentage)
                s = str(percentage) + "%"
                print(s)
                last_percentage = percentage

            self.time += self.hydro_driver.step() 

            if self.time - last_writing_time >= self.dt_writing:
                self.write_to_h5()
                last_writing_time = self.time

        self.h5file.close()
        print("Done!")
        print(f"Output file is {self.h5filename}")

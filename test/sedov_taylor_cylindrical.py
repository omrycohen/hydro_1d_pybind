# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Sedov Taylor
# %% [markdown]
# ## Generate data

# %%
import os 
import sys
import h5py as h5
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# get this file's ('test.py') path
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(f'{dir_path}/../')

# import simulation
import src.simulation as simulation

# %%
name = "Sedov Taylor Cylincrical"
geometry = simulation.geometries["cylindrical"]

num_of_cells   = 100
gamma          = 1.4

L              = 1.2
t_max          = 1.1

energy         = 0.311357

rho_atm        = 1.
pressure_atm   = 0.
first_cell_vol = geometry.beta / (geometry.alpha + 1) * (L / num_of_cells) ** (geometry.alpha + 1)
pressure_exp   = energy * (gamma - 1) / first_cell_vol

position = np.linspace(0.0, L, num_of_cells + 1)
velocity = np.zeros(num_of_cells + 1)

pressure = np.ones(num_of_cells) * pressure_atm
pressure[0] = pressure_exp

rho = np.ones(num_of_cells) * rho_atm

sim = simulation.Simulation(name = name,
                            geometry = geometry, 
                            gamma = gamma, 
                            init_position = position,
                            init_velocity = velocity,
                            init_pressure = pressure,
                            init_rho = rho,
                            dt_writing = 1e-3,
                            max_initial_dt = 1e-8 * t_max)
sim.mainloop(max_time = t_max)

# %% [markdown]
# ## Analyze data

# %%
from sedovpy import sedov

time_to_plot = 1.

# Extracting simulation data

f = h5.File(sim.h5filename, 'r')
sim_time = f["time"][:]
sim_time_index = np.argmin((sim_time - time_to_plot) ** 2)
time_plotted = sim_time[sim_time_index]
sim_rho_to_plot = f["rho"][sim_time_index, :]
sim_partition_pos = f["position"][sim_time_index, :]
sim_pos_to_plot = (sim_partition_pos[1:] + sim_partition_pos[:-1]) / 2

# Menahem's solver

benchmark = sedov.Sedov(geometry = "cylindrical",
                        gamma = gamma,
                        rho0 = rho_atm,
                        omega = 0.,
                        eblast = energy,
                        r0 = .75).solve(sim_pos_to_plot, time_plotted)


fig, ax = plt.subplots()

# Plotting the bemchmark output
ax.plot(sim_pos_to_plot, benchmark['density'], label='Benchmark output')

# Plotting the simulation output
ax.plot(sim_pos_to_plot, sim_rho_to_plot, label='Simulation output')

ax.set(xlabel='position', ylabel='density',
       title=f'Cylindrical Sedov Taylor problem, density at t={time_plotted}')
ax.grid()
ax.legend()

plt.show()


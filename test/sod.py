# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Sod Shock Tube
# %% [markdown]
# ## Generate data

# %%
import os 
import sys
import h5py as h5
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# get this file's ('test.py') path
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(f'{dir_path}/../')

# import simulation
import src.simulation as simulation

# %%
name = "Sod"
geometry = simulation.geometries["planar"]

gamma = 1.4
L = 1.0
t_max = 0.2
num_of_cells = 100

rho_left       = 1.0
pressure_left  = 1.0
rho_right      = 0.125
pressure_right = 0.1

position = np.linspace(0.0, L, num_of_cells + 1)
velocity = np.zeros(num_of_cells + 1)
mid_position = (position[1:] + position[:-1]) / 2.0

pressure = np.zeros(num_of_cells)
pressure[mid_position > L/2.0]  = pressure_right
pressure[mid_position <= L/2.0] = pressure_left

rho = np.zeros(num_of_cells)
rho[mid_position > L/2.0]  = rho_right
rho[mid_position <= L/2.0] = rho_left

sim = simulation.Simulation(name           = name,
                            geometry       = geometry, 
                            gamma          = gamma, 
                            init_position  = position,
                            init_velocity  = velocity,
                            init_pressure  = pressure,
                            init_rho       = rho,
                            dt_writing     = 1e-3,
                            max_initial_dt = 1e-8 * t_max)
sim.mainloop(max_time = t_max)

# %% [markdown]
# ## Analyze data

# %%
from simple_shock_tube_calculator.shocktubecalc.shocktubecalc import sod

time_to_plot = 0.2

# Extracting simulation data
f = h5.File(sim.h5filename, 'r')
sim_time = f["time"][:]
sim_time_index = np.argmin((sim_time - time_to_plot) ** 2)
time_plotted = sim_time[sim_time_index]
sim_rho_to_plot = f["rho"][sim_time_index, :]
sim_partition_pos = f["position"][sim_time_index, :]
sim_pos_to_plot = (sim_partition_pos[1:] + sim_partition_pos[:-1]) / 2

# Analytical solver
positions, regions, values = sod.solve(left_state=(1, 1, 0), right_state=(0.1, 0.125, 0.),
                                       geometry=(0., 1., 0.5), t=time_plotted, gamma=gamma, npts=500)


fig, ax = plt.subplots()

# Plotting the analytical solution
ax.plot(values['x'], values['rho'], color='r', label='Analytical solution')

# Plotting the simulation output
ax.plot(sim_pos_to_plot, sim_rho_to_plot, label='Simulation output')

ax.set(xlabel='position', ylabel='density',
       title=f'Sod problem, density at t={time_plotted}')
ax.grid()
ax.legend()

plt.show()


# %%


